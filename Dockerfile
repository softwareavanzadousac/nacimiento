FROM php:apache

RUN apt-get update
RUN apt-get install -y git
RUN docker-php-ext-install pdo pdo_mysql mysqli
RUN a2enmod rewrite
RUN curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
RUN chmod +x /usr/local/bin/phpunit
RUN sed -ri -e 's!80!9000!g' /etc/apache2/ports.conf
RUN sed -ri -e 's!80!9000!g' /etc/apache2/sites-enabled/000-default.conf

COPY . /var/www/html/

WORKDIR /var/www/html/
EXPOSE 9000



