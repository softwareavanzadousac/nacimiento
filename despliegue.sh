set -e

COMMIT_SHA1=$CI_COMMIT_SHORT_SHA

# We must export it so it's available for envsubst
export COMMIT_SHA1=$COMMIT_SHA1

# since the only way for envsubst to work on files is using input/output redirection,
#  it's not possible to do in-place substitution, so we need to save the output to another file
#  and overwrite the original with that one.
envsubst < kube/deployment.yml.template > kube/00-nacimiento-deployment.yml
kubectl apply -f kube/

