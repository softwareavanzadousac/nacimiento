<?php
class Persona_model extends MY_Model
{
    protected $table = 'Persona';

    public function __construct()
    {
        parent::__construct();
	}
	
	public function checkLogin($password)
	{
		return $this->db->insert($this->table, $password);
	}

	public function buscarPersona($dpi)
	{
		return $this->db->from($this->table)->where('dpi',$dpi)->count_all_results();
	}

	public function buscarDepto($depto)
	{
		return $this->db->from('Departamento')->where('llave',$depto)->count_all_results();
	}

	public function buscarMuni($depto, $muni)
	{
		return $this->db->from('Municipio m')
						->join('Departamento d','d.idDepartamento = m.departamento_id')->where('d.llave',$depto)->where('m.llave',$muni)
						->count_all_results();
	}

	public function obtenerDatos($dpi)
	{
		$existe = $this->db->from($this->table)->where('dpi',$dpi)->count_all_results();
		if($existe == 0){
			return new stdClass();
		}
		//return $this->db->select('idPersona, nombres, apellidos, fecha_defuncion')->from($this->table)->where('dpi',$dpi)->get()->row();
		return $this->db->select('y.idPersona acta, y.apellidos apellidos, y.nombres nombre, y.dpiPadre dpipadre, p.nombres nombrepadre, p.apellidos apellidopadre, y.dpiMadre dpimadre, m.nombres nombremadre, m.apellidos apellidomadre, y.fecha_nacimiento fechanac, d.nombre departamento, mu.nombre municipio, y.genero genero')
						->from($this->table.' y')
						->join('Persona p','p.dpi = y.dpiPadre')
						->join('Persona m','m.dpi = y.dpiMadre')
						->join('Departamento d','d.llave = y.departamento')
						->join('Municipio mu','mu.llave = y.municipio')
						->where('y.dpiPadre = '.$dpi.' OR y.dpiMadre ='.$dpi.' AND d.idDepartamento = mu.departamento_id')
						->get()->result_array();
	}
}

/*

+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| ID          | int(11)      | NO   | PRI | NULL    | auto_increment |
| AREA_TITULO | varchar(150) | NO   |     | NULL    |                |
| AREA_CLAVE  | varchar(150) | YES  |     | NULL    |                |
| AREA_STATUS | tinyint(1)   | YES  |     | 1       |                |
+-------------+--------------+------+-----+---------+----------------+

 */
