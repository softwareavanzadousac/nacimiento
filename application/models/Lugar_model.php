<?php
class Lugar_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
	}
	
	public function getDepartamentos()
	{
		return $this->db->select('llave, nombre')->from('Departamento')->get()->result_array();
	}

	public function getMunicipios($llave)
	{
		if($llave == ''){
			return new stdClass();
		}
		//return $this->db->select('idMunicipio, nombre')->from('Municipio')->where('departamento_id',$departamento_id)->get()->result_array();

		return $this->db->from('Municipio m')
						->select("m.llave llave, m.nombre nombre",false)
						->join('Departamento d','d.idDepartamento = m.departamento_id AND d.idDepartamento = '.$llave)
						->get()->result_array();

	}
}

