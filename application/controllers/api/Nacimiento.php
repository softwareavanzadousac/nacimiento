<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require_once APPPATH . 'libraries/JWT.php';
use \Firebase\JWT\JWT;
class Nacimiento extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
			die();
		}
		$this->load->model('persona_model','persona');
	}

	public function index_post()
	{   
		$_POST = json_decode(file_get_contents("php://input"), true);
		$existe_madre = 0;
		$existe_padre = 0;
		$persona = -1;

		$dpi_madre = $this->input->post('dpimadre');
		if($dpi_madre != ''){
			$madre = $this->persona->buscarPersona((int)$dpi_madre);
			if($madre == 0){
				$respuesta = array(
					'estado' => '500',
					'mensaje' => 'El DPI de la madre ('.$dpi_madre.') no existe'
				);
				$this->response($respuesta, REST_Controller::HTTP_OK);
			}
			$existe_madre = 1;
		}

		$dpi_padre = $this->input->post('dpipadre');
		if($dpi_padre != ''){
			$padre = $this->persona->buscarPersona((int)$dpi_padre);
			if($padre == 0){
				$respuesta = array(
					'estado' => '500',
					'mensaje' => 'El DPI del padre ('.$dpi_padre.') no existe'
				);
				$this->response($respuesta, REST_Controller::HTTP_OK);
			}
			$existe_padre = 1;
		}

		$depto = $this->input->post('departamento');
		$existe_depto = $this->persona->buscarDepto($depto);
		if($existe_depto == 0){
			$respuesta = array(
				'estado' => '500',
				'mensaje' => 'El código del departamento ('.$depto.') no existe'
			);
			$this->response($respuesta, REST_Controller::HTTP_OK);
		}

		$muni = $this->input->post('municipio');
		$existe_muni = $this->persona->buscarMuni($depto,$muni);
		if($existe_muni == 0){
			$respuesta = array(
				'estado' => '500',
				'mensaje' => 'El código del municipio ('.$muni.') no existe para ese departamento ('.$depto.')'
			);
			$this->response($respuesta, REST_Controller::HTTP_OK);
		}

		if($existe_madre == 1 && $existe_padre == 1){
			$data = array(
				'nombres' => $this->input->post('nombre'),
				'apellidos' => $this->input->post('apellido'),
				'fecha_nacimiento' => $this->input->post('fechanacimiento'),
				'genero' => $this->input->post('genero'),
				'difunto' => 0,
				'estadocivil' => 'S',
				'municipio' => $muni,
				'departamento' => $depto,
				'dpipadre' => (int) $dpi_padre,
				'dpimadre' => (int) $dpi_madre
			);
			$persona = $this->persona->checkLogin($data);
		}
		elseif($existe_madre == 1 && $existe_padre == 0){
			$data = array(
				'nombres' => $this->input->post('nombre'),
				'apellidos' => $this->input->post('apellido'),
				'fecha_nacimiento' => $this->input->post('fechanacimiento'),
				'genero' => $this->input->post('genero'),
				'difunto' => 0,
				'estadocivil' => 'S',
				'municipio' => $muni,
				'departamento' => $depto,
				'dpimadre' => (int) $dpi_madre
			);
			$persona = $this->persona->checkLogin($data);
		}
		elseif($existe_madre == 0 && $existe_padre == 1){
			$data = array(
				'nombres' => $this->input->post('nombre'),
				'apellidos' => $this->input->post('apellido'),
				'fecha_nacimiento' => $this->input->post('fechanacimiento'),
				'genero' => $this->input->post('genero'),
				'difunto' => 0,
				'estadocivil' => 'S',
				'municipio' => $muni,
				'departamento' => $depto,
				'dpipadre' => (int) $dpi_padre
			);
			$persona = $this->persona->checkLogin($data);
		}
		else{
			$respuesta = array(
				'estado' => '500',
				'mensaje' => 'Se necesita al menos el DPI de un padre o madre'
			);
			$this->response($respuesta, REST_Controller::HTTP_OK);
		}
		
        if($persona == 1){
			$respuesta = array(
				'estado' => '200',
				'mensaje' => 'Acta creada existosamente'
			);
			$this->response($respuesta, REST_Controller::HTTP_OK);
		}
		else{
			$respuesta = array(
				'estado' => '500',
				'mensaje' => 'Hubo un error durante la creacion del acta'
			);
			$this->response($respuesta, REST_Controller::HTTP_OK);
		}

	}

	public function obtener_post()
	{   
		$_POST = json_decode(file_get_contents("php://input"), true);
		$dpi = (int) $this->input->post('dpi');
		$persona = $this->persona->obtenerDatos($dpi);

        if($persona  == new \stdClass()){
			$respuesta = array(
				'estado' => '500',
				'mensaje' => 'No se encuentra el DPI: '.$dpi
			);
			$this->response($respuesta, REST_Controller::HTTP_OK);
		}
		else{
			$this->response($persona, REST_Controller::HTTP_OK);
		}
	}
}
