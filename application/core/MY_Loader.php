<?php
/**
 * Template that loads header and footer automatically
 */
class MY_Loader extends CI_Loader {


    public function templateprivate($template_name, $vars = array(), $return = FALSE)
    {
        parent::__construct();

        $instance =& get_instance();

		if($return){
			$content  = $instance->load->view('private/layout/header', $vars, $return);
			$content  .= $instance->load->view($template_name, $vars, $return);
			$content  .= $instance->load->view('private/layout/footer', $vars, $return);
		}else{
			$instance->load->view('private/layout/header', $vars);
			$instance->load->view($template_name, $vars);
			$instance->load->view('private/layout/footer', $vars);
		}
    }

    public function template($template_name, $vars = array(), $return = FALSE)
    {
        parent::__construct();

        $instance =& get_instance();

		if($return){
			$content  = $instance->load->view('layout/header', $vars, $return);
			$content  .= $instance->load->view($template_name, $vars, $return);
			$content  .= $instance->load->view('layout/footer', $vars, $return);
		}else{
			$instance->load->view('layout/header', $vars);
			$instance->load->view($template_name, $vars);
			$instance->load->view('layout/footer', $vars);
		}
		
	}
	
}
